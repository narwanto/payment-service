package com.narwanto.payment.exception;

import com.narwanto.payment.dto.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
@ControllerAdvice
public class HealthCareExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		log.error("error general exception : ", ex);
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = ErrorResponse.of("Internal Server Error", details, HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleValidation(Exception ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		log.error("error ConstraintViolationException : ", ex);
		details.add(ex.getMessage());
		ErrorResponse error = ErrorResponse.of("Validation Error", details, HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NullPointerException.class)
	public final ResponseEntity<Object> handleNullPointerExceptions(Exception ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		log.error("error NullPointerException : ", ex);
		details.add(ex.getMessage());
		ErrorResponse error = ErrorResponse.of("Null Pointer Error", details, HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<Object> handleFileNotFoundExceptions(ResourceNotFoundException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		log.error("error FileNotFoundException : ", ex);
		details.add(ex.getMessage());
		ErrorResponse error = ErrorResponse.of("File Not Found Error", details, HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		log.error("error BadRequestAlertException : ", ex);
		details.add(ex.getLocalizedMessage());

		ErrorResponse error = ErrorResponse.of("Bad Request Exception", details, HttpStatus.BAD_REQUEST);
		log.error("error", ex);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse error = ErrorResponse.of("Bad Request", details, HttpStatus.BAD_REQUEST);
		log.error("error", ex);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
