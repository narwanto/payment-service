package com.narwanto.payment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 7924662556272424313L;

    public BadRequestException(String s) {
        super(s);
    }
}