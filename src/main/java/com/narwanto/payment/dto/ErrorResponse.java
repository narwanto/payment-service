package com.narwanto.payment.dto;

import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.List;

public class ErrorResponse {

	private final HttpStatus status;

	private final String message;

	private final Date timestamp;

	private List<String> details;
	
	protected ErrorResponse(final String message, HttpStatus status) {
		this.message = message;
		this.status = status;
		this.timestamp = new Date();
	}

	protected ErrorResponse(final String message, final List<String> details, HttpStatus status) {
		this.message = message;
		this.status = status;
		this.timestamp = new Date();
		this.details = details;
	}

	public static ErrorResponse of(final String message, HttpStatus status) {
		return new ErrorResponse(message, status);
	}

	public static ErrorResponse of(final String message, List<String> details, HttpStatus status) {
		return new ErrorResponse(message, details, status);
	}


	public Integer getStatus() {
		return status.value();
	}

	public String getMessage() {
		return message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}


}