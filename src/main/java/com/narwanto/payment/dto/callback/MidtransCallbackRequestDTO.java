package com.narwanto.payment.dto.callback;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.narwanto.payment.dto.feign.VaNumberDTO;
import com.narwanto.payment.validator.annotation.ValidMidtransCallback;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@ValidMidtransCallback
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class MidtransCallbackRequestDTO implements Serializable {

    @NotBlank
    private String orderId;
    @NotBlank
    private String grossAmount;
    private String transactionId;
    private String transactionTime;
    private String transactionStatus;
    @NotBlank
    private String statusCode;
    private String statusMessage;
    private String signatureKey;
    private String settlementTime;

    @NotNull
    private String paymentType;

    private String merchantId;
    private String fraudStatus;
    private String currency;

    private List<VaNumberDTO> vaNumbers;

}
