package com.narwanto.payment.dto.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class TransactionDataDTO implements Serializable {

    private String key;
    private String value;

    public TransactionDataDTO(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public TransactionDataDTO() {
    }
}
