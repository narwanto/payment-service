package com.narwanto.payment.dto.response;

import com.narwanto.payment.enums.PaymentCategory;
import lombok.Data;

import java.io.Serializable;

@Data
public class PaymentChannelResponseDTO implements Serializable {
    private String code;
    private PaymentCategory category;
    private String name;
    private String icon;
}
