package com.narwanto.payment.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.enums.PaymentStatus;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class OrderDetailResponseDTO implements Serializable {

    private String id;
    private String profileId;
    private OrderStatus status;
    private PaymentStatus paymentStatus;
    private String channelCode;
    private BigDecimal amountToPaid;
    private Long createdDate;

    private List<TransactionDataDTO> transactionData;

}
