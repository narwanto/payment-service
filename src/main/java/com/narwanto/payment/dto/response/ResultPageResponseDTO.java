package com.narwanto.payment.dto.response;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ResultPageResponseDTO<T> implements Serializable {

    private List<T> result;
    private Integer pages;
    private Long elements;

}
