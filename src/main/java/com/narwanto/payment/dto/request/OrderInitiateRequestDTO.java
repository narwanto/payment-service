package com.narwanto.payment.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.narwanto.payment.validator.annotation.ValidInitiatePayment;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ValidInitiatePayment
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class OrderInitiateRequestDTO implements Serializable {

    @NotBlank(message = "orderId must present")
    private String orderId;

    @NotBlank(message = "paymentChannel must present")
    private String paymentChannel;

}
