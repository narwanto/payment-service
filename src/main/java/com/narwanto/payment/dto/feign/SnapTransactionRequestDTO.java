package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class SnapTransactionRequestDTO implements Serializable {

    private TransactionDTO transactionDetails;
    private List<ItemDTO> itemDetails;
    private CustomerDTO customerDetails;
    private List<String> enabledPayments;
    private ExpiryDTO expiry;
    private CallbackDTO callbacks;

    private BcaVaDTO bcaVa;

    private String customField1;
    private String customField2;
    private String customField3;

}
