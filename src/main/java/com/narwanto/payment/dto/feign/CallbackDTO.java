package com.narwanto.payment.dto.feign;

import lombok.Data;

import java.io.Serializable;

@Data
public class CallbackDTO implements Serializable {

    private String finish;

    public CallbackDTO(String finish) {
        this.finish = finish;
    }

    public CallbackDTO() {
    }
}
