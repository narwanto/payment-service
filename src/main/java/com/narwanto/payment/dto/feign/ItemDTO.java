package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ItemDTO implements Serializable {

    private String id;
    private String name;
    private String brand;
    private String category;
    private String merchantName;
    private BigDecimal price;
    private Integer quantity;

}
