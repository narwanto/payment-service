package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TransactionDetailDTO implements Serializable {

    private String orderId;
    private String transactionTime;
    private BigDecimal grossAmount;
    private String currency;
    private String paymentType;
    private String signatureKey;
    private String statusCode;
    private String transactionId;
    private String transactionStatus;
    private String fraudStatus;
    private String statusMessage;
    private String merchantId;

    private List<VaNumberDTO> vaNumbers;

}
