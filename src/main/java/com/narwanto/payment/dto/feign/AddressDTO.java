package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AddressDTO extends CustomerInformation {

    private String address;
    private String city;
    private String postalCode;
    private String countryCode;

}
