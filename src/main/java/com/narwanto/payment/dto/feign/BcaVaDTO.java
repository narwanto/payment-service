package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class BcaVaDTO extends VirtualAccountDTO {

    private String subCompanyCode;
    private FreeText freeText;

    @Data
    public static class FreeText implements Serializable {
        private List<Text> inquiry;
        private List<Text> payment;
    }

    @Data
    public static class Text implements Serializable {
        private String en;
        private String id;
    }

}
