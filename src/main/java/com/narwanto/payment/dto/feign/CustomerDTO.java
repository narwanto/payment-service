package com.narwanto.payment.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CustomerDTO extends CustomerInformation {

    private AddressDTO billingAddress;
    private AddressDTO shippingAddress;

}
