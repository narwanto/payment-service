package com.narwanto.payment.util;

import com.narwanto.payment.enums.OrderStatus;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Constants {

    public static final String UTIL_CLASS = "utility class";

    public static class PaymentProperties {
        public static final String BANK_TRANSFER_PAYMENT = "bank_transfer_payment";
        public static final String GOPAY_PAYMENT = "gopay_payment";
        public static final String INQUIRY_TEXT = "Private Order";
        public static final String PAYMENT_TEXT = "Private Payment";

        public static final Map<String, String> BEAN_PAYMENT_MAP = Map.of(
                "bca_va", BANK_TRANSFER_PAYMENT,
                "gopay", GOPAY_PAYMENT,
                "bri_va", BANK_TRANSFER_PAYMENT,
                "bni_va", BANK_TRANSFER_PAYMENT,
                "permata_va", BANK_TRANSFER_PAYMENT
        );

        PaymentProperties() {
            throw new IllegalStateException(UTIL_CLASS);
        }
    }

    public static class MidTransProperties {
        public static final String TOKEN = "token";
        public static final String REDIRECT_LINK = "redirect_link";
        public static final String CLIENT_KEY = "client_key";
        public static final String BASE_URL = "base_url";
        public static final String MIN_EXPIRY_PAYMENT = "min_expiry_payment";
        public static final String VA_NUMBER = "va_number";

        public static final String TRANSACTION_TIME = "transactionTime";
        public static final String GROSS_AMOUNT = "grossAmount";
        public static final String STATUS_CODE = "statusCode";
        public static final String STATUS_MESSAGE = "statusMessage";
        public static final String SIGNATURE_KEY = "signatureKey";
        public static final String SETTLEMENT_TIME = "settlementTime";
        public static final String MERCHANT_ID = "merchantId";
        public static final String FRAUD_STATUS = "fraudStatus";

        MidTransProperties() {
            throw new IllegalStateException(UTIL_CLASS);
        }
    }

    public static class MidtransTransactionStatus {
        public static final String PENDING = "pending";
        public static final String EXPIRE = "expire";
        public static final String CAPTURE = "capture";
        public static final String SETTLEMENT = "settlement";
        public static final String DENY = "deny";
        public static final String CANCEL = "cancel";

        MidtransTransactionStatus() {
            throw new IllegalStateException(UTIL_CLASS);
        }
    }

    public static class OrderStatuses {
        public static final List<OrderStatus> CAN_EXPIRE = Collections.singletonList(OrderStatus.WAITING_CALLBACK);

        OrderStatuses() {
            throw new IllegalStateException(UTIL_CLASS);
        }
    }

    Constants() {
        throw new IllegalStateException(UTIL_CLASS);
    }
}
