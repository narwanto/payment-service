package com.narwanto.payment.util;

import com.narwanto.payment.config.MidtransProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Component
@AllArgsConstructor
public class MidtransUtil {

    private final MidtransProperties properties;

    public String generateSignature(String orderId, String statusCode, String amount) throws NoSuchAlgorithmException {

        String input = orderId + statusCode + amount + properties.getServerKey();

        MessageDigest md = MessageDigest.getInstance("SHA-512");

        byte[] messageDigest = md.digest(input.getBytes());

        BigInteger no = new BigInteger(1, messageDigest);

        StringBuilder hashText = new StringBuilder(no.toString(16));

        while (hashText.length() < 32) {
            hashText.insert(0, "0");
        }

        return hashText.toString();

    }

}
