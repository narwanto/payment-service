package com.narwanto.payment.util;

import com.narwanto.payment.dto.response.ResultPageResponseDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class HelperUtil {

    public static final String DEFAULT_DATE_TIME = "dd-MMM-yyyy hh:mm";
    public static final String MIDTRANS_FORMAT = "yyyy-MM-dd HH:mm:ss Z";
    public static final String DATE_FORMAT_DD_MMM_YYY = "dd-MMM-yyyy";

    public static String convertInstantToString(Instant instant) {
        return convertInstantToString(instant, DEFAULT_DATE_TIME);
    }
    public static String convertInstantToString(Instant instant, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format)
                .withZone(ZoneId.systemDefault());
        return formatter.format(instant);
    }

    public static <T> ResultPageResponseDTO<T> createResultPageDTO(List<T> data, Long totalElements, Integer pages){
        ResultPageResponseDTO<T> result = new ResultPageResponseDTO<>();
        result.setPages(pages);
        result.setElements(totalElements);
        result.setResult(data);
        return result;
    }

    public static Sort.Direction getSortBy(String sortBy){
        if(sortBy.equalsIgnoreCase("asc")) {
            return Sort.Direction.ASC;
        }else {
            return Sort.Direction.DESC;
        }
    }

    public static Sort getSort(String sortBy, String direction) {
        return Sort.by(Sort.Direction.valueOf(direction.toUpperCase()), sortBy);
    }

    public Pageable getPageable(Integer page, Integer limit, String sortBy, String direction) {
        return PageRequest.of(page, limit, getSort(sortBy, direction));
    }
}
