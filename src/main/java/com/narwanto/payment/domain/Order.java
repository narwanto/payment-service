package com.narwanto.payment.domain;

import com.narwanto.payment.enums.OrderStatus;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@DynamicUpdate
@Entity
@Data
@Table(name = "orders", 
indexes = { @Index(name = "uk_order_secure_id", columnList = "secure_id") })
public class Order extends AbstractAuditingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "profile_id", nullable = false)
	private String profileId;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private OrderStatus status;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	private List<OrderItem> items;
	
}
