package com.narwanto.payment.domain;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "order_transaction",
indexes = { @Index(name = "uk_order_transaction_secure_id", columnList = "secure_id") })
@Where(clause = "is_deleted=false")
public class OrderTransaction extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3502983359145531326L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	private Order order;
	
	@ManyToOne
	@JoinColumn(name = "channel_code", nullable = false)
	private PaymentChannel channel;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orderTransaction")
	private List<OrderTransactionActivity> activities;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orderTransaction")
	private List<OrderTransactionData> dataList;
}
