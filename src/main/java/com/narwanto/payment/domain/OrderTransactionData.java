package com.narwanto.payment.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "order_transaction_data")
public class OrderTransactionData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8435875291806241104L;

	@Id
	@Column(name="id", length=36)
	@Length(min=36, max=36)
    @GeneratedValue(generator= "system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")	
	private String id;
	
	@Column(name = "key", nullable = false)
	private String key;
	
	@Column(name = "value", nullable = false)
	private String value;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_transaction_id", nullable = false)
	private OrderTransaction orderTransaction;

	public OrderTransactionData(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public OrderTransactionData() {
	}
}
