package com.narwanto.payment.domain;

import com.narwanto.payment.enums.PaymentCategory;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "payment_channel")
public class PaymentChannel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 499144909359247456L;

	@Id
	private String code;

	@Enumerated(EnumType.STRING)
	@Column(name = "category", nullable = false)
	private PaymentCategory category;
	
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "icon")
	private String icon;

}
