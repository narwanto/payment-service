package com.narwanto.payment.domain;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * Base entity class
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = { @Index(name = "uk_secure_id", columnList = "secure_id") })
@Data
public abstract class AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = -7369920601847524273L;

    /**
     * Random id for security reason
     */
    @Column(name = "secure_id", unique = true, length = 36)	
    private String secureId= UUID.randomUUID().toString();
    
    @CreatedDate
    @Column(name = "created_date",nullable = false, updatable = false)
    private Instant createdDate = Instant.now();
    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;
    @LastModifiedDate
    @Column(name = "modified_date")
    private Instant modifiedDate = Instant.now();
    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;
    @Version
    @Column(name = "version")
    private Integer version;
    
    @Column(name = "is_deleted")
    private Boolean deleted;

    @PrePersist
    public void prePersist() {
    	this.deleted = Boolean.FALSE;
    }


}

