package com.narwanto.payment.domain;

import com.narwanto.payment.enums.PaymentStatus;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "order_transaction_activity")
@Data
public class OrderTransactionActivity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8680625601454565458L;

	@Id
	@Column(name="id", length=36)
	@Length(min=36, max=36)
    @GeneratedValue(generator= "system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")	
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_transaction_id", nullable = false)
	private OrderTransaction orderTransaction;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private PaymentStatus status;

	@Column(name = "tracking_time")
	private LocalDateTime trackingTime = LocalDateTime.now();

}
