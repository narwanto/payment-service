package com.narwanto.payment.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "callback_activity_data")
@Data
public class CallbackActivityData implements Serializable {

    private static final long serialVersionUID = -8680625601454565458L;

    @Id
    @Column(name = "id", length = 36)
    @Length(min = 36, max = 36)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "callback_activity_id", nullable = false, referencedColumnName = "id")
    private CallbackActivity callbackActivity;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    public CallbackActivityData(CallbackActivity callbackActivity, String key, String value) {
        this.callbackActivity = callbackActivity;
        this.key = key;
        this.value = value;
    }
}
