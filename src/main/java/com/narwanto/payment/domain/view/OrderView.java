package com.narwanto.payment.domain.view;

import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.enums.PaymentStatus;
import lombok.Data;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@Immutable
@Entity
@Table(name = "view_order")
public class OrderView implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "secure_id")
    private String secureId;

    @Column(name = "profile_id")
    private String profileId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrderStatus status;

    @Column(name = "amount_to_paid")
    private BigDecimal amountToPaid;

    @Column(name = "latest_activity_id")
    private String latestActivityId;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "tracking_time")
    private LocalDateTime trackingTime;

    @Column(name = "channel_code")
    private String channelCode;

    @Column(name = "created_date")
    private Instant createdDate;

}
