package com.narwanto.payment.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "callback_activity")
public class CallbackActivity implements Serializable {

    private static final long serialVersionUID = -8680625601454565458L;

    @Id
    @Column(name = "id", length = 36)
    @Length(min = 36, max = 36)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @Column(name = "tracking_time", nullable = false)
    private LocalDateTime trackingTime = LocalDateTime.now();

    @Column(name = "order_id", nullable = false)
    private String orderId;

    @Column(name = "payment_type", nullable = false)
    private String paymentType;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "currency")
    private String currency;

}
