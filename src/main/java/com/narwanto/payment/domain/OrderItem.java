package com.narwanto.payment.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "order_item", 
indexes = { @Index(name = "uk_order_item_secure_id", columnList = "secure_id") })
public class OrderItem extends AbstractAuditingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id")
	private Order order;
	
	@Column(name = "price", nullable = false)
	private BigDecimal basicPrice;
	
	@Column(name = "tax_amount", nullable = false)
	private BigDecimal taxAmount;
	
	@Column(name = "discount_amount", nullable = false)
	private BigDecimal discountAmount;
	
	@Column(name = "service_code", nullable = false)
	private String serviceCode;

}
