package com.narwanto.payment.enums;

public enum OrderStatus {
	
	WAITING_OF_PAYMENT,
	WAITING_CALLBACK,
	PAYMENT_COMPLETED,
	PAYMENT_EXPIRED

}
