package com.narwanto.payment.enums;

public enum PaymentStatus {
    PENDING, EXPIRED, PAID, REFUND, FREE
}
