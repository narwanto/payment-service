package com.narwanto.payment.web;

import com.narwanto.payment.dto.response.PaymentChannelResponseDTO;
import com.narwanto.payment.dto.response.ResultPageResponseDTO;
import com.narwanto.payment.service.PaymentChannelService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class PaymentChannelResource {

    private final PaymentChannelService service;

    @GetMapping(value = "v1/payment-channel", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageResponseDTO<PaymentChannelResponseDTO>> findPaymentChannel(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer limit,
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "asc") String direction,
            @RequestParam(value = "code", required = false) String code
    ) {

        return ResponseEntity.ok(service.findPaymentChannel(page, limit, sortBy, direction, code));

    }

}
