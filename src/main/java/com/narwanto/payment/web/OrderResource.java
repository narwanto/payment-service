package com.narwanto.payment.web;

import com.narwanto.payment.dto.feign.TransactionDetailDTO;
import com.narwanto.payment.dto.request.OrderCreateRequestDTO;
import com.narwanto.payment.dto.request.OrderInitiateRequestDTO;
import com.narwanto.payment.dto.response.OrderCreateResponseDTO;
import com.narwanto.payment.dto.response.OrderDetailResponseDTO;
import com.narwanto.payment.dto.response.OrderInitiateResponseDTO;
import com.narwanto.payment.service.MidtransService;
import com.narwanto.payment.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Map;

@Validated
@AllArgsConstructor
@RestController
public class OrderResource {

	private final OrderService orderService;
	private final MidtransService midtransService;
	
	@PostMapping(value = "/v1/order", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderCreateResponseDTO> createOrder(@RequestBody @Valid OrderCreateRequestDTO dto){
		return ResponseEntity.created(URI.create("/v1/order")).body(orderService.createOrder(dto));
	}

	@PutMapping(value = "/v1/order", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderInitiateResponseDTO> initiatePayment(@RequestBody @Valid OrderInitiateRequestDTO dto){
		return ResponseEntity.ok(orderService.initiatePayment(dto));
	}

	@GetMapping(value = "/v1/order/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDetailResponseDTO> findOrderDetail(@PathVariable String orderId){
		return ResponseEntity.ok(orderService.findOrderDetail(orderId));
	}

	@PutMapping(value = "/v1/order/{orderTransactionId}/payment/change")
	public ResponseEntity<Void> cancelPaymentChannel(@PathVariable String orderTransactionId){
		orderService.cancelPaymentChannel(orderTransactionId);
		return ResponseEntity.ok().build();
	}

	@GetMapping(value = "/v1/order/{orderId}/midtrans", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransactionDetailDTO> findOrderDetailMidtrans(@PathVariable String orderId){
		return ResponseEntity.ok(midtransService.findOrderDetailMidtrans(orderId));
	}


}
