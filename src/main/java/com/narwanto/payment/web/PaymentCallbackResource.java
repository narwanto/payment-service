package com.narwanto.payment.web;

import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.service.impl.AbstractPaymentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class PaymentCallbackResource {

    private final BeanFactory beanFactory;

    @PostMapping(value = "v1/callback/midtrans", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> midtransCallback(@RequestBody @Valid MidtransCallbackRequestDTO dto) {

        AbstractPaymentService service = (AbstractPaymentService) beanFactory
                .getBean(dto.getPaymentType() + "_payment");

        service.handleCallback(dto);

        return ResponseEntity.ok().build();
    }


}
