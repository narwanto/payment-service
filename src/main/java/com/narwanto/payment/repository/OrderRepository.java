package com.narwanto.payment.repository;

import com.narwanto.payment.domain.Order;
import com.narwanto.payment.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findBySecureIdAndStatusIn(String secureId, List<OrderStatus> statuses);

    @Query(value = "select sum(price) - sum(discount_amount) + sum(tax_amount) from order_item oi " +
            "where order_id = :orderId", nativeQuery = true)
    BigDecimal countTotalPaid(Long orderId);

    Boolean existsBySecureId(String secureId);

    Optional<Order> findBySecureIdAndStatus(String orderId, OrderStatus status);

    Optional<Order> findBySecureId(String orderId);

}
