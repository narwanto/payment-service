package com.narwanto.payment.repository;

import com.narwanto.payment.domain.view.OrderView;
import com.narwanto.payment.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface OrderViewRepository extends JpaRepository<OrderView, Long> {

    Optional<OrderView> findBySecureId(String secureId);

    Optional<OrderView> findBySecureIdAndStatusIn(String secureId, Set<OrderStatus> statuses);

}
