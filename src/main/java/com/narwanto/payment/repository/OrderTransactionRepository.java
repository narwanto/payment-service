package com.narwanto.payment.repository;

import com.narwanto.payment.domain.OrderTransaction;
import com.narwanto.payment.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderTransactionRepository extends JpaRepository<OrderTransaction, Long> {

    Optional<OrderTransaction> findBySecureIdAndOrderStatus(String secureId, OrderStatus orderStatus);

    Optional<OrderTransaction> findTop1ByOrderSecureIdAndOrderStatusOrderByCreatedDateDesc(
            String orderId, OrderStatus orderStatus);

}
