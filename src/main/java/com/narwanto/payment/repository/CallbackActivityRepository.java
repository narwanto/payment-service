package com.narwanto.payment.repository;

import com.narwanto.payment.domain.CallbackActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CallbackActivityRepository extends JpaRepository<CallbackActivity, String> {

}
