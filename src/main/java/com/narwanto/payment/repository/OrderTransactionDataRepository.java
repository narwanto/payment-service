package com.narwanto.payment.repository;

import com.narwanto.payment.domain.Order;
import com.narwanto.payment.domain.OrderTransactionData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface OrderTransactionDataRepository extends JpaRepository<OrderTransactionData, String> {

    @Query(value = "SELECT otd FROM OrderTransactionData otd " +
            "JOIN otd.orderTransaction ot " +
            "JOIN ot.order o " +
            "WHERE o.id = :id")
    List<OrderTransactionData> findByOrderId(Long id);

    @Query(value = "SELECT otd FROM OrderTransactionData otd " +
            "JOIN otd.orderTransaction ot " +
            "WHERE ot.order = :order " +
            "AND otd.key IN :keys")
    List<OrderTransactionData> findByOrderAndKeyIn(Order order, Set<String> keys);

    @Query(value = "SELECT otd FROM OrderTransactionData otd " +
            "JOIN otd.orderTransaction ot " +
            "WHERE ot.order = :order " +
            "AND otd.key = :key")
    Optional<OrderTransactionData> findByOrderAndKey(Order order, String key);

}
