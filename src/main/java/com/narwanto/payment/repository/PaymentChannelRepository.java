package com.narwanto.payment.repository;

import com.narwanto.payment.domain.PaymentChannel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentChannelRepository extends JpaRepository<PaymentChannel, String> {

    Page<PaymentChannel> findByCode(String code, Pageable pageable);

}
