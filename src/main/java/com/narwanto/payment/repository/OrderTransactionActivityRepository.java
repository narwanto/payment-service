package com.narwanto.payment.repository;

import com.narwanto.payment.domain.Order;
import com.narwanto.payment.domain.OrderTransactionActivity;
import com.narwanto.payment.enums.PaymentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderTransactionActivityRepository extends JpaRepository<OrderTransactionActivity, String> {

    Optional<OrderTransactionActivity> findTop1ByOrderTransactionOrderAndStatusOrderByTrackingTimeDesc(
            Order order, PaymentStatus paymentStatus);

    @Query(value = "SELECT oa FROM OrderView ov " +
            "LEFT JOIN OrderTransactionActivity oa ON oa.id = ov.latestActivityId " +
            "WHERE ov.secureId = :orderId " +
            "AND oa.status = :status")
    Optional<OrderTransactionActivity> findLatestActivity(String orderId, PaymentStatus status);

}
