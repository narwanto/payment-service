package com.narwanto.payment.repository;

import com.narwanto.payment.domain.CallbackActivityData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CallbackActivityDataRepository extends JpaRepository<CallbackActivityData, String> {

}
