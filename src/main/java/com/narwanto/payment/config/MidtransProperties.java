package com.narwanto.payment.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "app.midtrans")
public class MidtransProperties {

    private String merchantId;
    private String clientKey;
    private String serverKey;
    private String callbackUrl;
    private String subCompanyCode;
    private Integer minExpiryPayment;

}
