package com.narwanto.payment.config;

import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Base64;

@Configuration
public class MidtransFeignConfig {

    @Bean
    public RequestInterceptor apiKey(MidtransProperties properties) {

        String key = properties.getServerKey() + ":";
        String encodedString = Base64.getEncoder().encodeToString(key.getBytes());

        return requestTemplate -> requestTemplate.header("Authorization", "Basic " + encodedString);
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
