package com.narwanto.payment.validator.annotation;

import com.narwanto.payment.validator.InitiatePaymentValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = InitiatePaymentValidator.class)
@Target({TYPE, ANNOTATION_TYPE, METHOD, PARAMETER, FIELD, CONSTRUCTOR, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidInitiatePayment {

    String message() default "order.or.payment.channel.invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
