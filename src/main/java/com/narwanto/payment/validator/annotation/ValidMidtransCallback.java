package com.narwanto.payment.validator.annotation;

import com.narwanto.payment.validator.MidtransCallbackValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = MidtransCallbackValidator.class)
@Target({TYPE, ANNOTATION_TYPE, METHOD, PARAMETER, FIELD, CONSTRUCTOR, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidMidtransCallback {

    String message() default "invalid.dto";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
