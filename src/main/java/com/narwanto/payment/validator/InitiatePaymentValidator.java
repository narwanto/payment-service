package com.narwanto.payment.validator;

import com.narwanto.payment.domain.Order;
import com.narwanto.payment.domain.PaymentChannel;
import com.narwanto.payment.dto.request.OrderInitiateRequestDTO;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.repository.OrderRepository;
import com.narwanto.payment.repository.PaymentChannelRepository;
import com.narwanto.payment.validator.annotation.ValidInitiatePayment;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collections;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Component
public class InitiatePaymentValidator implements ConstraintValidator<ValidInitiatePayment, OrderInitiateRequestDTO> {

    private final HttpSession httpSession;
    private final PaymentChannelRepository channelRepository;
    private final OrderRepository orderRepository;

    @SneakyThrows
    @Override
    public boolean isValid(OrderInitiateRequestDTO dto, ConstraintValidatorContext context) {

        Optional<PaymentChannel> paymentChannel = channelRepository.findById(dto.getPaymentChannel());
        if (paymentChannel.isEmpty()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("payment.channel.invalid").addConstraintViolation();
            return false;
        }

        Optional<Order> order = orderRepository.findBySecureIdAndStatusIn(dto.getOrderId(),
                Collections.singletonList(OrderStatus.WAITING_OF_PAYMENT));

        if (order.isEmpty()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("order.invalid.or.expired").addConstraintViolation();
            return false;
        }

        httpSession.setAttribute("order", order.get());
        httpSession.setAttribute("channel", paymentChannel.get());

        return true;
    }
}
