package com.narwanto.payment.validator;

import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.util.MidtransUtil;
import com.narwanto.payment.validator.annotation.ValidMidtransCallback;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
@AllArgsConstructor
@Component
public class MidtransCallbackValidator implements ConstraintValidator<ValidMidtransCallback, MidtransCallbackRequestDTO> {

    private final MidtransUtil midtransUtil;

    @SneakyThrows
    @Override
    public boolean isValid(MidtransCallbackRequestDTO dto, ConstraintValidatorContext context) {

        String signature = midtransUtil.generateSignature(dto.getOrderId(), dto.getStatusCode(), dto.getGrossAmount());

        if (!signature.equals(dto.getSignatureKey())) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("invalid.signature").addConstraintViolation();
            return false;
        }

        return true;
    }
}
