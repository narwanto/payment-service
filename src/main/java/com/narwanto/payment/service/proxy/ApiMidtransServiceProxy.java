package com.narwanto.payment.service.proxy;

import com.narwanto.payment.config.MidtransFeignConfig;
import com.narwanto.payment.dto.feign.TransactionDetailDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@FeignClient(name = "apiMidtransProxy", url = "${app.midtrans.api-base-url}", configuration = MidtransFeignConfig.class)
public interface ApiMidtransServiceProxy {

    @GetMapping("v2/{orderId}/status")
    TransactionDetailDTO getTransactionStatus(@PathVariable String orderId);

    @PostMapping("v2/{orderId}/cancel")
    Optional<TransactionDetailDTO> cancelTransaction(@PathVariable String orderId);
}
