package com.narwanto.payment.service.proxy;

import com.narwanto.payment.config.MidtransFeignConfig;
import com.narwanto.payment.dto.feign.SnapTransactionRequestDTO;
import com.narwanto.payment.dto.response.SnapTransactionResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "appMidtransProxy", url = "${app.midtrans.app-base-url}", configuration = MidtransFeignConfig.class)
public interface AppMidtransServiceProxy {

    @PostMapping(value = "snap/v1/transactions", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    SnapTransactionResponseDTO generateSnapToken(@RequestBody SnapTransactionRequestDTO dto);

    @PostMapping(value = "snap/v1/transactions", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    SnapTransactionResponseDTO generateSnapToken(@RequestHeader("X-Append-Notification") String callbackUrl,
                                                 @RequestBody SnapTransactionRequestDTO dto);


}
