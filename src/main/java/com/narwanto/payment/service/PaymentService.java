package com.narwanto.payment.service;

import com.narwanto.payment.domain.Order;
import com.narwanto.payment.domain.PaymentChannel;

import java.math.BigDecimal;
import java.util.Map;

public interface PaymentService {

    Map<String, Object> initiatePayment(Order order, BigDecimal amountToPaid, PaymentChannel channel);

}
