package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.dto.feign.VaNumberDTO;
import com.narwanto.payment.enums.*;
import com.narwanto.payment.service.OrderActivityService;
import com.narwanto.payment.service.PaymentService;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public abstract class AbstractPaymentService implements PaymentService {

    private final OrderActivityService orderActivityService;



    public abstract void handleCallback(MidtransCallbackRequestDTO dto);

    public abstract void cancelPayment(String secureId);

    void saveInitiatePaymentTransaction(Order order, PaymentChannel channel, List<OrderTransactionData> transactionData,
                                        String transactionId) {

        orderActivityService.saveInitiatePaymentTransaction(order, channel, transactionData, transactionId);

    }

    void updateTransactionData(Order order, Map<String, String> data) {
        orderActivityService.updateTransactionData(order, data);
    }

    void addTransactionData(OrderTransaction orderTransaction, String key, String value) {
        orderActivityService.addTransactionData(orderTransaction, key, value);
    }

    void completedOrder(Order order, PaymentStatus paymentStatus) {

        orderActivityService.completedOrder(order, paymentStatus);

    }

    public OrderTransaction findOrderByTransactionId(String transactionId, OrderStatus status) {
        return orderActivityService.findOrderByTransactionIdAndStatus(transactionId, status);
    }

    public CallbackActivity saveCallbackActivity(String orderId, String paymentType, String currency,
                                                 String transactionStatus, String transactionId) {

        CallbackActivity activity = new CallbackActivity();
        activity.setOrderId(orderId);
        activity.setPaymentType(paymentType);
        activity.setCurrency(currency);
        activity.setTransactionStatus(transactionStatus);
        activity.setTransactionId(transactionId);

        return activity;
    }

    public void saveActivityData(CallbackActivity activity, Map<String, String> activityData) {
        orderActivityService.saveCallbackActivity(activity, activityData);
    }

    protected void completedTransaction(Order order) {
        order.setStatus(OrderStatus.PAYMENT_COMPLETED);

        completedOrder(order, PaymentStatus.PAID);

    }


    protected void pendingTransaction(MidtransCallbackRequestDTO dto, Order order) {
        if (null == dto.getVaNumbers()) {
            return;
        }

        Map<String, String> vaNumbers = new HashMap<>();

        for (VaNumberDTO va : dto.getVaNumbers()) {
            vaNumbers.put(va.getBank().concat("_va_number"), va.getVaNumber());
        }

        updateTransactionData(order, vaNumbers);

    }

    protected void expireTransaction(Order order) {

        order.setStatus(OrderStatus.PAYMENT_EXPIRED);

        completedOrder(order, PaymentStatus.EXPIRED);

    }

}
