package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.enums.PaymentStatus;
import com.narwanto.payment.repository.OrderRepository;
import com.narwanto.payment.service.OrderActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

@Slf4j
@Service("free_payment")
public class FreePaymentServiceImpl extends AbstractPaymentService {

    @Autowired
    public FreePaymentServiceImpl(OrderRepository orderRepository, OrderActivityService orderActivityService) {
        super(orderActivityService);
    }

    @Override
    public Map<String, Object> initiatePayment(Order order, BigDecimal amountToPaid, PaymentChannel channel) {

        log.debug("free payment");
        order.setStatus(OrderStatus.PAYMENT_COMPLETED);

        completedOrder(order, PaymentStatus.FREE);

        return null;
    }

    @Override
    public void handleCallback(MidtransCallbackRequestDTO dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void cancelPayment(String secureId) {
        throw new UnsupportedOperationException();
    }
}
