package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.PaymentChannel;
import com.narwanto.payment.dto.response.PaymentChannelResponseDTO;
import com.narwanto.payment.dto.response.ResultPageResponseDTO;
import com.narwanto.payment.repository.PaymentChannelRepository;
import com.narwanto.payment.service.PaymentChannelService;
import com.narwanto.payment.util.HelperUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class PaymentChannelServiceImpl implements PaymentChannelService {

    private final PaymentChannelRepository paymentChannelRepository;

    @Override
    public ResultPageResponseDTO<PaymentChannelResponseDTO> findPaymentChannel(
            Integer page, Integer limit, String sortBy, String direction, String code) {

        Sort sort = Sort.by(Sort.Direction.valueOf(direction.toUpperCase()), sortBy);
        Pageable pageable = PageRequest.of(page, limit, sort);

        Page<PaymentChannel> channelPage = null == code || code.isEmpty() ? paymentChannelRepository.findAll(pageable)
                : paymentChannelRepository.findByCode(code, pageable);

        List<PaymentChannelResponseDTO> responseDTOS = channelPage.stream().map(c -> {
            PaymentChannelResponseDTO dto = new PaymentChannelResponseDTO();
            dto.setCategory(c.getCategory());
            dto.setCode(c.getCode());
            dto.setName(c.getName());
            dto.setIcon(c.getIcon());
            return dto;
        }).collect(Collectors.toList());

        return HelperUtil.createResultPageDTO(responseDTOS, channelPage.getTotalElements(), channelPage.getTotalPages());
    }
}
