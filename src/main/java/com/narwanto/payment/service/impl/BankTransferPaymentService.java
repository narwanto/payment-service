package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.dto.feign.VaNumberDTO;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.service.MidtransService;
import com.narwanto.payment.service.OrderActivityService;
import com.narwanto.payment.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.narwanto.payment.util.Constants.MidtransTransactionStatus.*;

@Slf4j
@Service(Constants.PaymentProperties.BANK_TRANSFER_PAYMENT)
public class BankTransferPaymentService extends AbstractPaymentService {

    private final MidtransService midtransService;

    @Autowired
    public BankTransferPaymentService(OrderActivityService orderActivityService,
                                      MidtransService midtransService) {
        super(orderActivityService);
        this.midtransService = midtransService;
    }

    @Override
    public Map<String, Object> initiatePayment(Order order, BigDecimal amountToPaid, PaymentChannel channel) {

        log.debug("bank transfer transaction");

        String transactionId = UUID.randomUUID().toString();

        List<OrderTransactionData> transactionData = new ArrayList<>();
        Map<String, Object> response = midtransService.initiateSnapTransaction(transactionId,
                amountToPaid, channel.getCode(),
                "085727053566", "mockemail@mailinator.com",
                transactionData);

        saveInitiatePaymentTransaction(order, channel, transactionData, transactionId);

        return response;
    }

    @Override
    public void handleCallback(MidtransCallbackRequestDTO dto) {

        log.info("midtrans bank transfer callback {}", dto);

        OrderTransaction orderTransaction = findOrderByTransactionId(dto.getOrderId(), OrderStatus.WAITING_CALLBACK);

        Map<String, String> data = midtransService.constructTransactionData(dto);

        for (VaNumberDTO va : dto.getVaNumbers()) {
            addTransactionData(orderTransaction, va.getBank().concat("_va_number"), va.getVaNumber());
            data.put(va.getBank().concat("_va_number"), va.getVaNumber());
        }

        CallbackActivity activity = saveCallbackActivity(dto.getOrderId(), dto.getPaymentType(), dto.getCurrency(),
                dto.getTransactionStatus(), dto.getTransactionId());

        saveActivityData(activity, data);

        switch (dto.getTransactionStatus()) {
            case PENDING: {
                pendingTransaction(dto, orderTransaction.getOrder());
                break;
            }

            case CANCEL:
            case EXPIRE: {
                expireTransaction(orderTransaction.getOrder());
                break;
            }

            case CAPTURE:
            case SETTLEMENT: {
                completedTransaction(orderTransaction.getOrder());
                break;
            }

            default:
                break;
        }

    }

    @Override
    public void cancelPayment(String orderTransactionId) {

        midtransService.cancelTransaction(orderTransactionId);

    }

}
