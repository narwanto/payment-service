package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.service.MidtransService;
import com.narwanto.payment.service.OrderActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.narwanto.payment.util.Constants.MidtransTransactionStatus.*;

@Slf4j
@Service("gopay_payment")
public class GopayPaymentService extends AbstractPaymentService {

    private final MidtransService midtransService;

    public GopayPaymentService(OrderActivityService orderActivityService,
                               MidtransService midtransService) {
        super(orderActivityService);
        this.midtransService = midtransService;
    }

    @Override
    public Map<String, Object> initiatePayment(Order order, BigDecimal amountToPaid, PaymentChannel channel) {

        String transactionId = UUID.randomUUID().toString();

        log.debug("Gopay transaction");

        List<OrderTransactionData> transactionData = new ArrayList<>();
        Map<String, Object> response = midtransService.initiateSnapTransaction(transactionId,
                amountToPaid, channel.getCode(),
                "085727053566", "mockemail@mailinator.com",
                transactionData);

        saveInitiatePaymentTransaction(order, channel, transactionData, transactionId);

        return response;
    }

    @Transactional
    @Override
    public void handleCallback(MidtransCallbackRequestDTO dto) {

        log.info("midtrans gopay callback {}", dto);

        OrderTransaction orderTransaction = findOrderByTransactionId(dto.getOrderId(), OrderStatus.WAITING_CALLBACK);

        CallbackActivity activity = saveCallbackActivity(dto.getOrderId(), dto.getPaymentType(), dto.getCurrency(),
                dto.getTransactionStatus(), dto.getTransactionId());

        saveActivityData(activity, midtransService.constructTransactionData(dto));

        switch (dto.getTransactionStatus()) {
            case PENDING: {
                pendingTransaction(dto, orderTransaction.getOrder());
                break;
            }

            case CANCEL:
            case EXPIRE: {
                expireTransaction(orderTransaction.getOrder());
                break;
            }

            case CAPTURE:
            case SETTLEMENT: {
                completedTransaction(orderTransaction.getOrder());
                break;
            }

            default:
                break;
        }

    }

    @Override
    public void cancelPayment(String secureId) {

        midtransService.cancelTransaction(secureId);

    }
}
