package com.narwanto.payment.service.impl;

import com.narwanto.payment.config.MidtransProperties;
import com.narwanto.payment.domain.OrderTransactionData;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.dto.feign.*;
import com.narwanto.payment.dto.response.SnapTransactionResponseDTO;
import com.narwanto.payment.service.MidtransService;
import com.narwanto.payment.service.proxy.ApiMidtransServiceProxy;
import com.narwanto.payment.service.proxy.AppMidtransServiceProxy;
import com.narwanto.payment.util.Constants;
import com.narwanto.payment.util.HelperUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Slf4j
@Service
@AllArgsConstructor
public class MidtransServiceImpl implements MidtransService {

    private final AppMidtransServiceProxy appMidtrans;
    private final ApiMidtransServiceProxy apiMidtrans;
    private final MidtransProperties properties;

    @Override
    public Map<String, Object> initiateSnapTransaction(String orderId, BigDecimal amountToPaid, String channel,
                                                       String mobileNumber, String email, List<OrderTransactionData> transactionData) {

        SnapTransactionRequestDTO snap = new SnapTransactionRequestDTO();

        snap.setTransactionDetails(new TransactionDTO(orderId, amountToPaid));
        snap.setEnabledPayments(Collections.singletonList(channel));

        CustomerDTO customer = new CustomerDTO();
        customer.setEmail(email);
        customer.setPhone(mobileNumber);

        Map<String, Object> result = new HashMap<>();
        String vaNumber = null;

        if ("bca_va".equalsIgnoreCase(channel)) {

            if (null == mobileNumber || mobileNumber.length() <= 11) {
                vaNumber = "12345678911";
            } else {
                vaNumber = mobileNumber.substring(mobileNumber.length() - 11);
            }

            BcaVaDTO bcaVaDTO = new BcaVaDTO();
            bcaVaDTO.setSubCompanyCode(properties.getSubCompanyCode());
            bcaVaDTO.setVaNumber(vaNumber);

            BcaVaDTO.FreeText freeText = new BcaVaDTO.FreeText();

            BcaVaDTO.Text inquiry = new BcaVaDTO.Text();
            BcaVaDTO.Text payment = new BcaVaDTO.Text();

            inquiry.setId(Constants.PaymentProperties.INQUIRY_TEXT);
            inquiry.setEn(Constants.PaymentProperties.INQUIRY_TEXT);

            payment.setId(Constants.PaymentProperties.PAYMENT_TEXT);
            payment.setEn(Constants.PaymentProperties.PAYMENT_TEXT);

            freeText.setPayment(Collections.singletonList(payment));
            freeText.setInquiry(Collections.singletonList(inquiry));

            bcaVaDTO.setFreeText(freeText);

            snap.setBcaVa(bcaVaDTO);

            String fullVaNumber = String.format("%s%s", properties.getSubCompanyCode(), vaNumber);
            result.put("va_number", fullVaNumber);

        }

        Instant now = Instant.now();

        ExpiryDTO expiry = new ExpiryDTO();
        expiry.setUnit("minutes");
        expiry.setDuration(properties.getMinExpiryPayment());
        expiry.setStartTime(HelperUtil.convertInstantToString(now, HelperUtil.MIDTRANS_FORMAT));

        snap.setExpiry(expiry);

        SnapTransactionResponseDTO response = appMidtrans.generateSnapToken(properties.getCallbackUrl(), snap);

        result.put(Constants.MidTransProperties.TOKEN, response.getToken());
        result.put(Constants.MidTransProperties.REDIRECT_LINK, response.getRedirectUrl());
        result.put(Constants.MidTransProperties.MIN_EXPIRY_PAYMENT, properties.getMinExpiryPayment());
        result.put(Constants.MidTransProperties.CLIENT_KEY, properties.getClientKey());
        result.put(Constants.MidTransProperties.MERCHANT_ID, properties.getMerchantId());

        log.debug("success initiate transaction {}", result);

        String expiryString = HelperUtil.convertInstantToString(Instant.now().plus(properties.getMinExpiryPayment(), ChronoUnit.MINUTES));

        if (null != vaNumber) {
            transactionData.add(new OrderTransactionData("bca_va_number", vaNumber));
        }
        transactionData.add(new OrderTransactionData(Constants.MidTransProperties.REDIRECT_LINK, response.getRedirectUrl()));
        transactionData.add(new OrderTransactionData(Constants.MidTransProperties.CLIENT_KEY, properties.getClientKey()));
        transactionData.add(new OrderTransactionData(Constants.MidTransProperties.TOKEN, response.getToken()));
        transactionData.add(new OrderTransactionData(Constants.MidTransProperties.MIN_EXPIRY_PAYMENT, expiryString));

        return result;
    }

    @Override
    public Map<String, String> constructTransactionData(MidtransCallbackRequestDTO dto) {
        Map<String, String> data = new HashMap<>();
        data.put(Constants.MidTransProperties.TRANSACTION_TIME, dto.getTransactionTime());
        data.put(Constants.MidTransProperties.GROSS_AMOUNT, dto.getGrossAmount());
        data.put(Constants.MidTransProperties.STATUS_CODE, dto.getStatusCode());
        data.put(Constants.MidTransProperties.STATUS_MESSAGE, dto.getStatusMessage());
        data.put(Constants.MidTransProperties.SIGNATURE_KEY, dto.getSignatureKey());
        data.put(Constants.MidTransProperties.SETTLEMENT_TIME, dto.getSettlementTime());
        data.put(Constants.MidTransProperties.MERCHANT_ID, dto.getMerchantId());
        data.put(Constants.MidTransProperties.FRAUD_STATUS, dto.getFraudStatus());
        return data;
    }

    @Override
    public void cancelTransaction(String orderTransactionId) {
        apiMidtrans.cancelTransaction(orderTransactionId);
    }

    @Override
    public TransactionDetailDTO findOrderDetailMidtrans(String orderId) {

        return apiMidtrans.getTransactionStatus(orderId);

    }
}
