package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.domain.view.OrderView;
import com.narwanto.payment.dto.request.OrderCreateRequestDTO;
import com.narwanto.payment.dto.request.OrderInitiateRequestDTO;
import com.narwanto.payment.dto.response.OrderCreateResponseDTO;
import com.narwanto.payment.dto.response.OrderDetailResponseDTO;
import com.narwanto.payment.dto.response.OrderInitiateResponseDTO;
import com.narwanto.payment.dto.response.TransactionDataDTO;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.exception.ResourceNotFoundException;
import com.narwanto.payment.repository.OrderRepository;
import com.narwanto.payment.repository.OrderTransactionDataRepository;
import com.narwanto.payment.repository.OrderTransactionRepository;
import com.narwanto.payment.repository.OrderViewRepository;
import com.narwanto.payment.service.OrderService;
import com.narwanto.payment.util.Constants;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;
	private final HttpSession httpSession;
	private final BeanFactory beanFactory;
	private final OrderViewRepository orderViewRepository;
	private final OrderTransactionDataRepository orderTransactionDataRepository;
	private final OrderTransactionRepository orderTransactionRepository;

	@Override
	public OrderCreateResponseDTO createOrder(OrderCreateRequestDTO dto) {

		Order order = new Order();
		order.setProfileId(dto.getProfileId());
		order.setStatus(OrderStatus.WAITING_OF_PAYMENT);
		List<OrderItem> orderItems = dto.getOrders().stream().map(i -> {
			OrderItem oi = new OrderItem();
			oi.setBasicPrice(i.getBasicAmount());
			oi.setOrder(order);
			oi.setServiceCode(i.getServiceCode());
			oi.setTaxAmount(BigDecimal.ZERO);
			oi.setDiscountAmount(BigDecimal.ZERO);
			return oi;
		}).collect(Collectors.toList());
		order.setItems(orderItems);

		orderRepository.save(order);
		OrderCreateResponseDTO response = new OrderCreateResponseDTO();
		response.setOrderId(order.getSecureId());

		return response;
	}


	@Transactional
	@Override
	public OrderInitiateResponseDTO initiatePayment(OrderInitiateRequestDTO dto) {

		Order order = (Order) httpSession.getAttribute("order");
		PaymentChannel channel = (PaymentChannel) httpSession.getAttribute("channel");

		BigDecimal amountToPaid = orderRepository.countTotalPaid(order.getId());

		String paymentMethod = amountToPaid.compareTo(BigDecimal.ZERO) > 0 ?
				Constants.PaymentProperties.BEAN_PAYMENT_MAP.get(channel.getCode().toLowerCase()) : "free_payment";

		AbstractPaymentService paymentMethodService = (AbstractPaymentService) beanFactory.getBean(paymentMethod);

		Map<String, Object> paymentProperty = paymentMethodService.initiatePayment(order, amountToPaid, channel);

		OrderInitiateResponseDTO result = new OrderInitiateResponseDTO();
		result.setOrderId(order.getSecureId());
		result.setPayment(paymentProperty);

		return result;
	}

	@Override
	public OrderDetailResponseDTO findOrderDetail(String orderId) {

		OrderView orderView = orderViewRepository.findBySecureId(orderId)
				.orElseThrow(() -> new ResourceNotFoundException("order.not.found"));

		OrderDetailResponseDTO response =  new OrderDetailResponseDTO();
		BeanUtils.copyProperties(orderView, response);
		response.setId(orderView.getSecureId());
		response.setCreatedDate(orderView.getCreatedDate().toEpochMilli());

		List<OrderTransactionData> transactionData = orderTransactionDataRepository.findByOrderId(orderView.getId());

		response.setTransactionData(transactionData.stream()
				.map(d -> new TransactionDataDTO(d.getKey(), d.getValue()))
				.collect(Collectors.toList()));

		return response;
	}

	@Override
	public void cancelPaymentChannel(String orderId) {

		OrderTransaction latestOrderTransaction = orderTransactionRepository
				.findTop1ByOrderSecureIdAndOrderStatusOrderByCreatedDateDesc(orderId, OrderStatus.WAITING_CALLBACK)
				.orElseThrow(() -> new ResourceNotFoundException("order.transaction.not.found"));

		latestOrderTransaction.setDeleted(true);
		orderTransactionRepository.save(latestOrderTransaction);

		PaymentChannel channel = latestOrderTransaction.getChannel();

		AbstractPaymentService paymentService = (AbstractPaymentService) beanFactory.
				getBean(Constants.PaymentProperties.BEAN_PAYMENT_MAP.get(channel.getCode()));

		paymentService.cancelPayment(latestOrderTransaction.getSecureId());

		Order order = latestOrderTransaction.getOrder();
		order.setStatus(OrderStatus.WAITING_OF_PAYMENT);

		orderRepository.save(order);

	}


}
