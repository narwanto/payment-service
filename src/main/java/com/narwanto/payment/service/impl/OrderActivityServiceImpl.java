package com.narwanto.payment.service.impl;

import com.narwanto.payment.domain.*;
import com.narwanto.payment.enums.OrderStatus;
import com.narwanto.payment.enums.PaymentStatus;
import com.narwanto.payment.exception.ResourceNotFoundException;
import com.narwanto.payment.repository.*;
import com.narwanto.payment.service.OrderActivityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class OrderActivityServiceImpl implements OrderActivityService {

    private final OrderTransactionRepository orderTransactionRepository;
    private final OrderTransactionActivityRepository orderTransactionActivityRepository;
    private final OrderTransactionDataRepository orderTransactionDataRepository;
    private final OrderRepository orderRepository;
    private final CallbackActivityRepository callbackActivityRepository;
    private final CallbackActivityDataRepository callbackActivityDataRepository;

    @Override
    public void saveInitiatePaymentTransaction(Order order, PaymentChannel channel,
                                               List<OrderTransactionData> transactionData, String transactionId) {

        OrderTransaction transaction = new OrderTransaction();
        transaction.setSecureId(transactionId);

        transaction.setOrder(order);
        transaction.setChannel(channel);

        transaction = orderTransactionRepository.save(transaction);

        saveTransactionData(transaction, transactionData);

        OrderTransactionActivity initiateActivity = new OrderTransactionActivity();
        initiateActivity.setStatus(PaymentStatus.PENDING);
        initiateActivity.setOrderTransaction(transaction);

        orderTransactionActivityRepository.save(initiateActivity);

        order.setStatus(OrderStatus.WAITING_CALLBACK);
        orderRepository.save(order);

    }

    private void saveTransactionData(OrderTransaction transaction, List<OrderTransactionData> transactionData) {

        transactionData.forEach(t -> t.setOrderTransaction(transaction));

        orderTransactionDataRepository.saveAll(transactionData);
    }

    @Override
    public void updateTransactionData(Order order, Map<String, String> data) {

        Map<String, OrderTransactionData> existing = orderTransactionDataRepository.findByOrderAndKeyIn(order, data.keySet())
                .stream().collect(Collectors.toMap(OrderTransactionData::getKey, Function.identity()));

        List<OrderTransactionData> toSaved = new ArrayList<>();

        for (var entry : existing.entrySet()) {
            if (data.containsKey(entry.getKey())) {
                OrderTransactionData td = entry.getValue();
                td.setValue(data.getOrDefault(entry.getKey(), td.getValue()));
                toSaved.add(td);
            }
        }

        orderTransactionDataRepository.saveAll(toSaved);

    }

    @Override
    public void addTransactionData(OrderTransaction orderTransaction, String key, String value) {
        Optional<OrderTransactionData> exists = orderTransactionDataRepository.findByOrderAndKey(orderTransaction.getOrder(), key);
        if (exists.isEmpty()) {
            OrderTransactionData td = new OrderTransactionData(key, value);
            td.setOrderTransaction(orderTransaction);
            orderTransactionDataRepository.save(td);
            return;
        }
        exists.get().setValue(value);
        orderTransactionDataRepository.save(exists.get());
    }

    @Override
    public void completedOrder(Order order, PaymentStatus paymentStatus) {

        orderRepository.save(order);

        if (PaymentStatus.FREE.equals(paymentStatus)) {
            log.debug("free payment completed paid with order {}", order.getSecureId());
            return;
        }

        saveTransactionActivity(order, paymentStatus);

    }

    private void saveTransactionActivity(Order order, PaymentStatus paymentStatus) {
        Optional<OrderTransactionActivity> latest = orderTransactionActivityRepository
                .findLatestActivity(order.getSecureId(), PaymentStatus.PENDING);

        if (latest.isEmpty()) {
            return;
        }

        OrderTransactionActivity completedActivity = new OrderTransactionActivity();
        completedActivity.setStatus(paymentStatus);
        completedActivity.setOrderTransaction(latest.get().getOrderTransaction());

        orderTransactionActivityRepository.save(completedActivity);
    }

    @Override
    public void completedOrder(String orderId, OrderStatus completedOrderStatus, PaymentStatus completedPaymentStatus) {

        Optional<Order> order = orderRepository.findBySecureId(orderId);
        if (order.isEmpty()) {
            return;
        }

        order.get().setStatus(completedOrderStatus);
        orderRepository.save(order.get());

        saveTransactionActivity(order.get(), completedPaymentStatus);

    }

    @Override
    public void saveCallbackActivity(CallbackActivity activity, Map<String, String> activityData) {
        activity = callbackActivityRepository.save(activity);
        List<CallbackActivityData> toSaved = new ArrayList<>();
        for (var entry : activityData.entrySet()) {
            if (null != entry.getValue()) {
                CallbackActivityData data = new CallbackActivityData(activity, entry.getKey(), entry.getValue());
                toSaved.add(data);
            }
        }

        callbackActivityDataRepository.saveAll(toSaved);
    }

    @Override
    public OrderTransaction findOrderByTransactionIdAndStatus(String transactionId, OrderStatus orderStatus) {

        return orderTransactionRepository.findBySecureIdAndOrderStatus(transactionId, orderStatus)
                .orElseThrow(() -> new ResourceNotFoundException("transaction.order.not.found"));

    }
}
