package com.narwanto.payment.service;


import com.narwanto.payment.dto.response.PaymentChannelResponseDTO;
import com.narwanto.payment.dto.response.ResultPageResponseDTO;

public interface PaymentChannelService {

    ResultPageResponseDTO<PaymentChannelResponseDTO> findPaymentChannel(
            Integer page, Integer limit, String sortBy, String direction, String code);

}
