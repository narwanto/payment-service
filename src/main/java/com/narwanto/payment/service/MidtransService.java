package com.narwanto.payment.service;

import com.narwanto.payment.domain.OrderTransactionData;
import com.narwanto.payment.dto.callback.MidtransCallbackRequestDTO;
import com.narwanto.payment.dto.feign.TransactionDetailDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface MidtransService {

    Map<String, Object> initiateSnapTransaction(String orderId, BigDecimal amountToPaid, String channel,
                                                String mobileNumber, String email, List<OrderTransactionData> transactionData);

    Map<String, String> constructTransactionData(MidtransCallbackRequestDTO dto);

    void cancelTransaction(String orderTransactionId);

    TransactionDetailDTO findOrderDetailMidtrans(String orderId);
}
