package com.narwanto.payment.service;

import com.narwanto.payment.dto.request.OrderCreateRequestDTO;
import com.narwanto.payment.dto.request.OrderInitiateRequestDTO;
import com.narwanto.payment.dto.response.OrderCreateResponseDTO;
import com.narwanto.payment.dto.response.OrderDetailResponseDTO;
import com.narwanto.payment.dto.response.OrderInitiateResponseDTO;

public interface OrderService {

	OrderCreateResponseDTO createOrder(OrderCreateRequestDTO dto);

	OrderInitiateResponseDTO initiatePayment(OrderInitiateRequestDTO dto);

    OrderDetailResponseDTO findOrderDetail(String orderId);

	void cancelPaymentChannel(String orderId);

}
