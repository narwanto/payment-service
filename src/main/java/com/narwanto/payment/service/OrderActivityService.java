package com.narwanto.payment.service;


import com.narwanto.payment.domain.*;
import com.narwanto.payment.enums.*;

import java.util.List;
import java.util.Map;

public interface OrderActivityService {

    void saveInitiatePaymentTransaction(Order order, PaymentChannel channel,
                                        List<OrderTransactionData> transactionData, String transactionId);

    void updateTransactionData(Order order, Map<String, String> data);

    void addTransactionData(OrderTransaction orderTransaction, String key, String value);

    void completedOrder(Order order, PaymentStatus completedPaymentStatus);

    void completedOrder(String orderId, OrderStatus completedOrderStatus,PaymentStatus completedPaymentStatus);

    void saveCallbackActivity(CallbackActivity activity, Map<String, String> activityData);

    OrderTransaction findOrderByTransactionIdAndStatus(String transactionId, OrderStatus orderStatus);

}
