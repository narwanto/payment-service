CREATE TABLE public.payment_channel (
    code varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    icon varchar(255) NULL,
    category varchar(255) not null,
    CONSTRAINT payment_channel_pkey PRIMARY KEY (code)
);

CREATE TABLE public.orders (
    id bigserial NOT NULL,
    created_by varchar(255) NOT NULL,
    created_date timestamp NOT NULL,
    is_deleted bool NULL,
    modified_by varchar(255) NULL,
    modified_date timestamp NULL,
    secure_id varchar(36) NULL,
    "version" int4 NULL,
    profile_id varchar(255) NULL,
    status varchar(255) NOT NULL,
    CONSTRAINT orders_pkey PRIMARY KEY (id),
    CONSTRAINT uk_4prqkbd8jmlalhilu8yo71wdq UNIQUE (secure_id)
);

CREATE INDEX uk_order_secure_id ON public.orders (secure_id);

CREATE TABLE public.order_item (
    id bigserial NOT NULL,
    created_by varchar(255) NOT NULL,
    created_date timestamp NOT NULL,
    is_deleted bool NULL,
    modified_by varchar(255) NULL,
    modified_date timestamp NULL,
    secure_id varchar(36) NULL,
    "version" int4 NULL,
    price numeric(19, 2) NOT NULL,
    discount_amount numeric(19, 2) NOT NULL,
    service_code varchar(255) NOT NULL,
    tax_amount numeric(19, 2) NOT NULL,
    order_id int8 NOT NULL,
    CONSTRAINT order_item_pkey PRIMARY KEY (id),
    CONSTRAINT uk_sl6yu4g5rrstwxgn9fk9qd1g6 UNIQUE (secure_id),
    CONSTRAINT fkt4dc2r9nbvbujrljv3e23iibt FOREIGN KEY (order_id) REFERENCES public.orders(id)
);
CREATE INDEX uk_order_item_secure_id ON public.order_item (secure_id);

CREATE TABLE public.order_transaction (
    id bigserial NOT NULL,
    created_by varchar(255) NOT NULL,
    created_date timestamp NOT NULL,
    is_deleted bool NULL,
    modified_by varchar(255) NULL,
    modified_date timestamp NULL,
    secure_id varchar(36) NULL,
    "version" int4 NULL,
    status varchar(255) NULL,
    channel_code varchar(255) NOT NULL,
    order_id int8 NOT NULL,
    CONSTRAINT order_transaction_pkey PRIMARY KEY (id),
    CONSTRAINT uk_2anq2l97mhsn0le12iklo4kjj UNIQUE (secure_id),
    CONSTRAINT fkdlxpuxwvtnmfbdieiltacv7cf FOREIGN KEY (order_id) REFERENCES public.orders(id),
    CONSTRAINT fkkwocwhdhsceowitbi1g616g0v FOREIGN KEY (channel_code) REFERENCES public.payment_channel(code)
);
CREATE INDEX uk_order_transaction_secure_id ON public.order_transaction(secure_id);

CREATE TABLE public.order_transaction_activity (
    id varchar(36) NOT NULL,
    status varchar(255) NOT NULL,
    order_transaction_id int8 NOT NULL,
    tracking_time timestamp not null default now(),
    CONSTRAINT order_transaction_activity_pkey PRIMARY KEY (id),
    CONSTRAINT fkbdnb3eycnrnrr0rfypmg97dsc FOREIGN KEY (order_transaction_id) REFERENCES public.order_transaction(id)
);

CREATE TABLE public.order_transaction_data (
    id varchar(36) NOT NULL,
    "key" varchar(255) NOT NULL,
    value varchar(255) NOT NULL,
    order_transaction_id int8 NOT NULL,
    CONSTRAINT order_transaction_data_pkey PRIMARY KEY (id),
    CONSTRAINT fktkqjsfk65abd4vowlff15sgwp FOREIGN KEY (order_transaction_id) REFERENCES public.order_transaction(id)
);


INSERT INTO payment_channel (code, name, category, icon)
SELECT 'bca_va' , 'BCA Virtual account payment', 'ONLINE', 'a2c049db-e358-43bf-a4ce-5ffbf842bebe';

INSERT INTO payment_channel (code, name, category, icon)
SELECT 'gopay' , 'GoPay', 'ONLINE', 'a2c049db-e358-43bf-a4ce-5ffbf842bebe';

INSERT INTO payment_channel (code, name, category, icon)
SELECT 'bri_va' , 'BRIVA payment', 'ONLINE', 'a2c049db-e358-43bf-a4ce-5ffbf842bebe';

INSERT INTO payment_channel (code, name, category, icon)
SELECT 'bni_va' , 'BNI Virtual account payment', 'ONLINE', 'a2c049db-e358-43bf-a4ce-5ffbf842bebe';

INSERT INTO payment_channel (code, name, category, icon)
SELECT 'permata_va' , 'PERMATA Virtual account payment', 'ONLINE', 'a2c049db-e358-43bf-a4ce-5ffbf842bebe';

CREATE VIEW view_order as
SELECT o.*
     , (select channel_code from order_transaction ot where ot.order_id = o.id and ot.is_deleted = false order by created_date desc limit 1) as channel_code
, (select oa.id from order_transaction_activity oa
	join order_transaction ot on (ot.id = oa.order_transaction_id and ot.is_deleted = false)
	where ot.order_id = o.id
	order by oa.tracking_time desc limit 1) as latest_activity_id
, (select oa.status from order_transaction_activity oa
	join order_transaction ot on (ot.id = oa.order_transaction_id and ot.is_deleted = false)
	where ot.order_id = o.id and ot.is_deleted = false
	order by oa.tracking_time desc limit 1) as payment_status
, (select oa.tracking_time from order_transaction_activity oa
	join order_transaction ot on (ot.id = oa.order_transaction_id and ot.is_deleted = false)
	where ot.order_id = o.id
	order by oa.tracking_time desc limit 1) as tracking_time
, (select sum(price) - sum(discount_amount) + sum(tax_amount) from order_item oi where order_id = o.id) amount_to_paid
FROM orders o;